//
//  HWController.as
//
//  Created by Jim Mazur on 11/17/13.
//	contactjim@jimmazur.com
//

#include "RGBButton.as"

const int8 firstPad = 20;
const uint8 firstSeqSelector = 37;

enum F1Controls
{
	Filter1 = 2,
	Filter2 = 3,
	Filter3 = 4,
	Filter4 = 5,
	Volume1 = 6,
	Volume2 = 7,
	Volume3 = 8,
	Volume4 = 9,
	Sync = 12,
	Quant = 13,
	Capture = 14,
	Reverse = 15,
	Type = 16,
	Size = 17,
	Browse = 18,
	Pad_A1 = 10,
	Pad_B1 = 11,
	Pad_C1 = 12,
	Pad_D1 = 13,
	Pad_A2 = 14,
	Pad_B2 = 15,
	Pad_C2 = 16,
	Pad_D2 = 17,
	Pad_A3 = 18,
	Pad_B3 = 19,
	Pad_C3 = 20,
	Pad_D3 = 21,
	Pad_A4 = 22,
	Pad_B4 = 23,
	Pad_C4 = 24,
	Pad_D4 = 25,
	MuteA = 37,
	MuteB = 38,
	MuteC = 39,
	MuteD = 40
}

// Interface for any page 
interface F1PageFunctions
{
	void handlePadEvent(const uint8 &in pad, bool pressed);
	void updatePads();
	void setActive();
}

// Base class for any page, handles setting up the pads
class F1PageBase
{
	F1PageBase(){}
	F1PageBase(const string &in outputPort, const uint8 &in firstPad, const Color &in color)
	{
		m_outgoingHwMidiPort = outputPort;
		for (int i = 0; i < 16; i++)
		{
			RGBButton button(m_outgoingHwMidiPort, i + firstPad, color);
			m_arrayRGBButtons.insertLast(button);
		}
	}

	void setActive(bool isActive)
	{
		m_isActive = isActive;
		if (isActive) setActive();
	}

	void setActive()
	{

	}

	bool isActive ()
	{
		return m_isActive;
	}

	protected bool m_isActive = false;
	protected array<RGBButton> m_arrayRGBButtons;
	protected string m_outgoingHwMidiPort;
	protected HWController@ m_hwControllerParent;
}


// A page which is designed to display the sequencer
class SequencerPage : F1PageBase, F1PageFunctions
{
	SequencerPage(const string &in outputPort, const uint8 &in firstPad, const Color &in color)
	{
		// Connect to the base class (F1PageBase)
		super(outputPort, firstPad, color);
	}

	void handlePadEvent(const uint8 &in pad, bool pressed)
	{
		if (!m_isActive) return; // Page not active
		if (pressed)
		{
			m_sequencer.toggleStepState(pad);
		}
	}

	void setActive()
	{
		updatePads();
	}

	void updatePads()
	{
		for (uint i = 0; i < m_arrayRGBButtons.length(); i++)
		{
			updatePad(i);
		}
	}

	private void updatePad(const uint pad)
	{
		if (!m_isActive) return; // Dont update a pad if the page isn't active
		bool stepActive = false;
		stepActive = m_sequencer.isStepActive(pad);
		if (pad == m_currentStep)
		{
			m_arrayRGBButtons[pad].setColor(Color().getWhite());
		}
		else if (stepActive)
		{
			m_arrayRGBButtons[pad].setOn();
		}
		else
		{
			m_arrayRGBButtons[pad].setOff();
		}
	}

	void connectSequencer(Sequencer @sequencer)
	{
		@m_sequencer = @sequencer;
	}

	Sequencer@ getConnectedSequencer () const
	{
		return @m_sequencer;
	}

	void stepStateChanged(const uint step)
	{
		updatePad(step);
	}

	void setCurrentStep(const uint step)
	{
		m_lastStep = m_currentStep;
		m_currentStep = step;
		updatePad(m_currentStep);
		updatePad(m_lastStep);
	}

	private uint m_currentStep = 0;
	private uint m_lastStep = 0;
	private Sequencer@ m_sequencer;
	private RGBButton@ sequencerButton;
}



class HWController
{

	HWController(const string &in incomingHwMidiPort, const string &in outgoingHwMidiPort, const string &in outgoingSeqMidiPort)
	{
		m_incomingHwMidiPort = incomingHwMidiPort;
		m_outgoingHwMidiPort = outgoingHwMidiPort;
		m_outgoingSeqMidiPort = outgoingSeqMidiPort;

		SequencerPage page1(m_outgoingHwMidiPort, 10, Color(ChartreuseYellow));
		m_arrayOfSequencerPages.insertLast(@page1);
		SequencerPage page2(m_outgoingHwMidiPort, 10, Color(Lime));
		m_arrayOfSequencerPages.insertLast(@page2);
		SequencerPage page3(m_outgoingHwMidiPort, 10, Color(Aqua));
		m_arrayOfSequencerPages.insertLast(@page3);
		SequencerPage page4(m_outgoingHwMidiPort, 10, Color(Blue));
		m_arrayOfSequencerPages.insertLast(@page4);

		Button muteA(m_outgoingHwMidiPort, MuteA, Bright, Off);
		Button muteB(m_outgoingHwMidiPort, MuteB, Bright, Off);
		Button muteC(m_outgoingHwMidiPort, MuteC, Bright, Off);
		Button muteD(m_outgoingHwMidiPort, MuteD, Bright, Off);
		m_arraySeqSelects.insertLast(muteA);
		m_arraySeqSelects.insertLast(muteB);
		m_arraySeqSelects.insertLast(muteC);
		m_arraySeqSelects.insertLast(muteD);
	}
	~HWController()
	{
		// Delete references, therefore deleting the objects
		for (uint i = 0; i < m_arrayOfSequencerPages.length; i++)
			@m_arrayOfSequencerPages[i] = null;
	}

	// =====================================================================
	// Incoming HW Event Processing
	// =====================================================================

	void processMidiMessage (const MidiMessage &in message)
	{
		const int noteOrControl = message.getNoteOrControl();
		const bool pressed = message.isOn();

		if (noteOrControl >= MuteA && noteOrControl <= MuteD) // Mute buttons
		{
			uint page = noteOrControl - MuteA; // Get 0-based index
			handlePageSwitchEvent(page, pressed);
		}
		if (noteOrControl >= Pad_A1 && noteOrControl <= Pad_D4) // Pads
		{
			uint pad = noteOrControl - Pad_A1;
			handlePadEvent(pad, pressed);
		}
		if (noteOrControl >= Filter1 && noteOrControl <= Browse)
		{
			sendSeqMidiMessage(message);
		}
	}

	void handlePageSwitchEvent(const uint page, bool buttonpressed)
	{
		if (buttonpressed)
		{
			setSequencerPageAsActive(page);
		}
	}

	void handlePadEvent(const uint pad, bool buttonpressed)
	{
		for (uint i = 0; i < m_arrayOfSequencerPages.length(); i++)
		{
			m_arrayOfSequencerPages[i].handlePadEvent(pad, buttonpressed);
		}
	}

	// Cycles through all the pages and sets the desired page as active, meanwhile
	// deactivating all other pages. Additionally this sets the correct LED feedback 
	// on the bottom 4 Mute buttons.
	void setSequencerPageAsActive (const uint page)
	{
		for (uint i = 0; i < m_arrayOfSequencerPages.length(); i++)
		{
			if (page == i) // Is this the page to be turned on?
			{
				m_arrayOfSequencerPages[i].setActive(true);
				m_arraySeqSelects[i].setOn();
			}
			else
			{
				m_arrayOfSequencerPages[i].setActive(false);
				m_arraySeqSelects[i].setOff();
			}
		}
	}

	// =====================================================================
	// Incoming Sequencer Event Processing
	// =====================================================================

	void connectSequencer (const uint8 index, Sequencer @sequencer)
	{
		m_arrayOfSequencerPages[index].connectSequencer(@sequencer);
	}

	// Returns a pointer to the SequencerPage that contains the Sequencer
	// If the Sequencer page is not active, this will return null and prohibit
	// any functions being called
	SequencerPage@ findSequencer (Sequencer @sequencer)
	{
		for (uint i = 0; i < m_arrayOfSequencerPages.length(); i++)
		{
			if ( m_arrayOfSequencerPages[i].getConnectedSequencer() is sequencer )
				return @m_arrayOfSequencerPages[i];
		}
		return null;
	}

	void setCurrentStep (Sequencer @sequencer, const uint currentStep)
	{
		SequencerPage@ page = findSequencer(sequencer);
		if (@page == null) return;
		page.setCurrentStep(currentStep);
	}

	void stepStateChanged (Sequencer @sequencer, const uint step)
	{
		SequencerPage@ page = findSequencer(sequencer);
		if (@page == null) return;
		page.stepStateChanged(step);
	}

	void sendHWMidiMessage (const MidiMessage &in message)
	{
		sendMidiMessage(m_outgoingHwMidiPort, message);
	}

	void sendSeqMidiMessage (const MidiMessage &in message)
	{
		sendMidiMessage(m_outgoingSeqMidiPort, message);
	}

	array<SequencerPage@> m_arrayOfSequencerPages;
	array<Button> m_arraySeqSelects;

	private string m_outgoingSeqMidiPort;
	private string m_incomingHwMidiPort;
	private string m_outgoingHwMidiPort;
}