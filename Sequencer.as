//
//  Sequencer.as
//
//  Created by Jim Mazur on 11/17/13.
//	contactjim@jimmazur.com
//

// =====================================================================
// Step Object
// The lowest level of a sequence. Each step can be active, which means
// it is engaged and should have its state broadcasted when the sequencer
// reaches that step.
// =====================================================================

class Step
{
	Step() 
	{
		m_isActive = false;
	}

	// Toggles if step is active
	bool toggleState()
	{
		if (m_isActive) m_isActive = false;
		else m_isActive = true;
		return m_isActive;
	}

	// Returns if step is active
	bool isActive() const
	{
		return this.m_isActive;
	}

	void setActive(const bool isActive)
	{
		m_isActive = isActive;
	}

	// =====================================================================

	private bool m_isActive = false;
	
}

// =====================================================================
// Sequence Object
// Holds a series of steps to be sequenced.
// =====================================================================

class Sequence
{
	Sequence(){}

	Sequence(MidiMessage &in messageToSequence)
	{
		m_midiMessage = messageToSequence;
		createSteps();
	}

	private void createSteps(const uint stepsToCreate = 16)
	{
		for (uint i = 0; i < 16; i++)
		{
			Step step;
			m_arrayOfSteps.insertLast(step);
		}
	}

	// =====================================================================
	// Accessors
	// =====================================================================

	bool toggleStepState(const uint stepToToggle)
	{
		return m_arrayOfSteps[stepToToggle].toggleState();
	}

	bool isActive(const uint stepToCheck) const
	{
		return m_arrayOfSteps[stepToCheck].isActive();
	}

	void setActive(const uint stepToSet, const bool stateToSet = true)
	{
		m_arrayOfSteps[stepToSet].setActive(stateToSet);
	}

	// Returns the Midi message
	MidiMessage getMidiMessage() const
	{
		return m_midiMessage;
	}

	// =====================================================================
	private array<Step> m_arrayOfSteps;
	private MidiMessage m_midiMessage;
}

class Sequencer
{
	Sequencer() {}

	Sequencer(const string &in incomingMidiPort, const string &in outgoingPort, const int noteToBeSequenced)
	{
		m_incomingMidiPort = incomingMidiPort;
		m_outgoingMidiPort = outgoingPort;
		// TODO: hardcoded to notes at the moment
		MidiMessage message(0x90, noteToBeSequenced, 0x7f);
		Sequence sequence(message);
		@m_sequence = @sequence;
	}

	void processMidiMessage(const MidiMessage &in message)
	{
		if (message.isStart()) restartSequence();
		if (message.isClock()) processMidiTick();
	}

	private void restartSequence()
	{
		//DBG("Sequencer - restart clock");
		m_tickCount = 0;
		m_currentStep = 0;
	}

	private void processMidiTick()
	{
		m_tickCount++;
		//DBG("Seqeuncer - tick: " + m_tickCount);

		//Hack! Remove later with delayed noteOff option
		// if (m_needsNoteOff)
		// {
		// 	sendSequencerMidiMessage(m_sequence.getMidiMessage().getAsNoteOrControllerOff());
		// 	m_needsNoteOff = false;
		// }
		if (m_tickCount >= m_stepSize)
		{
			m_tickCount = 0;
			incrementStep();
		}
		
	}

	private void incrementStep()
	{
		m_currentStep++;

		if (m_currentStep >= 16) m_currentStep = 0;

		// Checks if the current step is active
		if (m_sequence.isActive(m_currentStep))
			handleActiveStep();

		m_hwController.setCurrentStep(@this, m_currentStep);
	}

	private void handleActiveStep()
	{
		//DBG("Sequencer - Step is active:" + m_currentStep);
		//sendSequencerMidiMessage( m_sequence.getMidiMessage() );
		sendMidiMessage( m_outgoingMidiPort, m_sequence.getMidiMessage() );
		sendMidiMessageDelayed( m_outgoingMidiPort, m_sequence.getMidiMessage().getAsNoteOrControllerOff(), 5 );
	}

	private void sendSequencerMidiMessage (const MidiMessage &in message)
	{
		int byte1 = 0;
		int byte2 = 0;
		int byte3 = 0;
		message.getBytes(byte1, byte2, byte3);
		sendMidiMessage(m_outgoingMidiPort, byte1, byte2, byte3);
	}

	void connectHWController(HWController @hwController)
	{
		@m_hwController = @hwController;

	}

	void toggleStepState(const int stepToToggle)
	{
		bool state = m_sequence.toggleStepState(stepToToggle);
		m_hwController.stepStateChanged(@this, stepToToggle);
	}

	bool isStepActive(const int stepToCheck) const
	{
		if ( m_sequence.isActive(stepToCheck) ) return true;
		return false;
	}

	// =====================================================================
	private string m_outgoingMidiPort;
	private string m_incomingMidiPort;
	private Sequence@ m_sequence;
	private int m_stepSize = 6;
	private int m_currentStep = 0;
	private bool m_needsNoteOff = true;
	private int m_tickCount = 0;

	HWController@ m_hwController;
	
}






