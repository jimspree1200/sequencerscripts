//
//  Main.as
//
//  Created by Jim Mazur on 11/17/13.
//	contactjim@jimmazur.com
//
// Every script must have at least:
// startup(),
// void handleIncomingMidiMessage (const string &in name, int byte1, int byte2, int byte3)
// void timerCallback(const string &in timerName)
// in order to function
// This function is called when the script is first executed. Set global variables
// here, or prepare the script for other nifty things!

#include "included.as"

int aGlobal = 0;

class someclass
{
	someclass () {}
	int someclassint;
}

void testValues ()
{
	someclass testclass;
	bool abool = true;
	bool bbool = false;
	int8 i8max = 127;
	int8 i8min = -128;
	int16 i16max = 32767;
	int16 i16min = -32768;
	int imax = 2147483647;
	int imin = -2147483648;
	int64 i64max = 9223372036854775807;
	//int64 i64min = -9223372036854775808;
	uint8 u8max = 255;
	uint16 u16max = 65535;
	uint uimax =  4294967295;
	uint64 u64max = 18446744073709551615;
	float fmax = 33554430;
	someclass@ testclassptr = @testclass;
}

void bFunction()
{
	double aFuntionb = 1.2345;
	float aFunctionc = 6.54321;

}

void aFunction()
{
	bFunction();
	//int64 64int = 64;
	uint aFunction = 2;
	bool aFunction2 = true;
	float aFunction3 = 3;
}

void startup ()
{
	testValues();
	cFunction();
	int aLocal = 2147483647;
	aFunction();
}

// This function is called whenever a MIDI message arrives at an enabled input port.
// The input name will contain a string, with the name of the MIDI input port.
// The 3 bytes represent the MIDI data. In the case the MIDI data is less than 3
// bytes than (such as a single byte MIDI clock message) than the second and third bytes 
// will have a 0 value by default.

void handleIncomingMidiMessage (const string &in name, int byte1, int byte2, int byte3)
{
	DBG(name);
}

// This function is called whenever a timer is triggered from the application. Use the
// startTimer function to create a timer

void timerCallback(const string &in timerName)
{
	//DBG(timerName + ": Timer called")
}

// API QUICK REFERENCE
// bool createMidiOutputDevice (string &in)
// bool createMidiInputDevice (string &in)
// void startTimer(string &in name, int milliseconds, int repeats)
// bool openMidiOutputDevice(string &in)
// bool openMidiInputDevice(string &in)
// void sendMidiMessage(string &in, int, int, int)
// void sendMidiMessageBuffered(string &in, int, int, int)
// void setMidiBufferInterval(string &in, int)
// void DBG(string &in)


// Starts a timer with a name. This timer will trigger timerCallback() with at an interval
// deinfed with the milliseconds variable. If repeats is less than or equal to 0, than the
// timer will run until the script is stopped