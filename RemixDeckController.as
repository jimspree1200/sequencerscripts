enum Deck
{
	DeckA,
	DeckB,
	DeckC,
	DeckD
}
enum Slot
{
	Slot1,
	Slot2,
	Slot3,
	Slot4
}
enum CellState
{
	empty,
	loaded = 42,
	playing = 84,
	triggered = 127
}
enum DeckControls
{
	TriggerCell1,
	TriggerCell2,
	TriggerCell3,
	TriggerCell4,
	TriggerCell5,
	TriggerCell6,
	TriggerCell7,
	TriggerCell8,
	TriggerCell9,
	TriggerCell10,
	TriggerCell11,
	TriggerCell12,
	TriggerCell13,
	TriggerCell14,
	TriggerCell15,
	TriggerCell16,
	Volume,
	Mute,
	Filter
}

class RemixCell
{
	RemixCell(){}

	RemixCell(const int index) 
	{
		m_index = index;
	}

	void setState(const int state)
	{
		m_state = state;
	}

	bool isState(const int state) const
	{
		if (m_state == state) return true;
		return false;
	}

	int getState() const
	{
		return m_state;
	}

	private int m_index = 0;
	private int m_state = 0;
}

class RemixSlot
{
	RemixSlot(){}
	RemixSlot(const uint8 slot)
	{
		for (int i = 0; i < 16; i++)
		{
			RemixCell cell(i);
			m_arrayOfRemixCells.insertLast(cell);
		}
	}

	void setCellState(const uint8 cellIndex, const uint8 state)
	{
		m_arrayOfRemixCells[cellIndex].setState(state);
	}

	uint8 getCellState(const uint8 cellIndex)
	{
		return m_arrayOfRemixCells[cellIndex].getState();
	}

	bool isCellState(const uint8 cellIndex, const uint8 state)
	{
		return m_arrayOfRemixCells[cellIndex].isState(state);
	}

	uint8 getPlayingCell() const
	{
		for (uint i = 0; i < m_arrayOfRemixCells.length(); i++ )
		{
			m_arrayOfRemixCells[i].isState(playing);
		}
		return 0;	
	}
	// The column of cells
	array<RemixCell> m_arrayOfRemixCells;
	uint8 m_slotVolume = 127;
	uint8 m_slotFilter = 64;
	bool m_slotMute = false;
}

class RemixDeckController
{
	RemixDeckController(){}
	RemixDeckController(const string &in incomingMidiPort, const string &in outgoingMidiPort, const uint8 deck)
	{
		m_deck = deck;
		m_incomingMidiPort = incomingMidiPort;
		m_outgoingMidiPort = outgoingMidiPort;

		for (int i = 0; i < 4; i++)
		{
			RemixSlot slot(i);
			m_arrayOfRemixSlots.insertLast(slot);
		}
		getTraktorState();
	}

	uint8 getDeck() const
	{
		return m_deck;
	}

	void createArrayOfRemixSlots()
	{

	}

	void processMidiMessage(const MidiMessage &in message)
	{

		const uint8 channel = message.getChannel();
		if ( channel >= ( m_deck * 4 ) && channel <= ((m_deck * 4) + 4) )
		{
			const uint8 slot = channel - ( m_deck * 4 );
			processSlotMessage(slot, message.getNoteOrControl(), message.getValue());
		}
	}

	void processSlotMessage(const uint8 slot, const uint8 control, const uint8 value)
	{
		if (control >= TriggerCell1 && control <= TriggerCell16)
		{
			DBG("Slot: " + slot + ", Control: " + control + ", Value: " + value);
			m_arrayOfRemixSlots[slot].setCellState(control, value);
		}
	}

	void getTraktorState()
	{	
		//DBG(m_outgoingMidiPort);
		sendMidiMessageDelayed(m_outgoingMidiPort, MidiMessage(0xBF, 0x7F, 0x7F), 1000);
	}

	private string m_incomingMidiPort;
	private string m_outgoingMidiPort;
	private array<RemixSlot> m_arrayOfRemixSlots;
	private uint8 m_deck = 0;
}

