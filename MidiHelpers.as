
//
//  MidiHelpers.as
//
//  Created by Jim Mazur on 11/17/13.
//	contactjim@jimmazur.com
//
//  NOTE: Highly derived from JUCE::MidiMessage class for ease of porting

class MidiMessage
{
	MidiMessage() {};

	// Construct a MidiMessage based off another MidiMessage
	MidiMessage(const MidiMessage &in other)
	{
		this.m_byte1 = other.m_byte1;
		this.m_byte2 = other.m_byte2;
		this.m_byte3 = other.m_byte3;
	}

	// Create a MidiMessage based off of 3 bytes
	MidiMessage(const int &in byte1, const int &in byte2, const int &in byte3)
	{
		m_byte1 = byte1;
		m_byte2 = byte2;
		m_byte3 = byte3;
	}

	// Set the bytes of the MidiMessage manually
	void setBytes(const int &in byte1, const int &in byte2, const int &in byte3)
	{
		m_byte1 = byte1;
		m_byte2 = byte2;
		m_byte3 = byte3;
	}

	// Get the bytes of the MidiMessage
	void getBytes(int &out byte1, int &out byte2, int &out byte3) const
	{
		byte1 = m_byte1;
		byte2 = m_byte2;
		byte3 = m_byte3;
	}

	int getByte1() const
	{
		return m_byte1;
	}

	int getByte2() const
	{
		return m_byte2;
	}

	int getByte3() const
	{
		return m_byte3;
	}

	uint8 getValue() const
	{
		return m_byte3;
	}

	uint8 getChannel() const
	{
		//Strips away channel message
		//00001111 & (0x0F)
		//10010001   (0x91)
		//--------
		//00000001   (0x01)
		const uint8 channel = m_byte1 & 0x0F;
		return channel;
	}

	// Return the MidiMessage as a Note Off / Controller Off message
	MidiMessage getAsNoteOrControllerOff() const
	{
		const int channel = getChannel();
		int byte1;
		if (isNoteOnOrOff()) byte1 = 0x80 + channel;
		if (isController()) byte1 = m_byte1;
		int byte2 = m_byte2;
		int byte3 = 0x00;

		MidiMessage returnMessage(byte1, byte2, byte3);
		return returnMessage;
	}

	// Is this a clock message?
	bool isClock() const
	{
		if (m_byte1 == 0xF8) return true;
		return false;
	}

	// Is this message a MIDI start message?
	bool isStart() const
	{
		if (m_byte1 == 0xFA) return true;
		return false;
	}

	// It this message a MIDI note on or note off message?
	bool isNoteOnOrOff() const
	{
		//Strips away channel message
		//11110000 & (0xF0)
		//10010001   (0x91)
		//--------
		//10010000   (0x90)
		const int data = m_byte1 & 0xF0;

		if (data == 0x90 || data == 0x80) return true;
		return false;
	}

	// Is this message a Controller message?
	bool isController() const
	{
		const int data = m_byte1 & 0xF0;
		if (data == 0xB0) return true;
		return false;
	}

	// Is this a full value on message (either note or controller)?
	bool isOn() const
	{
		if (m_byte3 == 0x7F) return true;
		return false;
	}

	// Get the note or controller number from the message
	int getNoteOrControl() const
	{
		if ( isController() || isNoteOnOrOff() ) return m_byte2;
		// Should put assert here later!
		return 0x7F;
	}

	bool isSameNoteOrController(const MidiMessage &in messageToCheck)
	{
		if ( !( messageToCheck.isNoteOnOrOff() == this.isNoteOnOrOff() ) ||
			!( messageToCheck.isController() == this.isController() ) )
		{
			return false;
		}
		if ( messageToCheck.getChannel() != this.getChannel() ) return false;
		if ( messageToCheck.getByte2() != this.getByte2() ) return false;
		return true;
	}

	// =====================================================================
	// Defaults all bytes to 'Active Sense' messages
	private int m_byte1 = 0xFE;
	private int m_byte2 = 0xFE;
	private int m_byte3 = 0xFE;
}

bool connectMidiPorts(const string &in incomingMidiPort, const string &in outgoingMidiPort)
{
	bool portOpened = openMidiInputDevice(incomingMidiPort);
   	if (!portOpened)
   	{
   		DBG("Failed to open Midi Input: " + incomingMidiPort);
   		return false;
   	}
   	else
   		DBG("Opened Midi Input: " + incomingMidiPort);

   	portOpened = openMidiOutputDevice(outgoingMidiPort);
   	if (!portOpened)
   	{
   		DBG("Failed to open Midi Output: " + outgoingMidiPort);
   		return false;
   	}
   	else
   		DBG("Opened Midi Output: " + outgoingMidiPort);

   	return true;
}

bool createMidiPorts(const string &in incomingMidiPort, const string &in outgoingMidiPort)
{
	bool portOpened = createMidiInputDevice(incomingMidiPort);
	if (!portOpened)
   	{
   		DBG("Failed to create Midi Input: " + incomingMidiPort);
   		return false;
   	}
   	else
   		DBG("Created Midi Input: " + incomingMidiPort);
   	portOpened = createMidiOutputDevice(outgoingMidiPort);
   	if (!portOpened)
   	{
   		DBG("Failed to create Midi Output: " + outgoingMidiPort);
   		return false;
   	}
   	else
   		DBG("Created Midi Output: " + outgoingMidiPort);

   	return true;
}

void sendMidiMessage (const string &in port, const MidiMessage &in message)
{
	sendMidiMessage(port, message.getByte1(), message.getByte2(), message.getByte3());
}

void sendMidiMessageDelayed (const string &in port, const MidiMessage &in message, const uint delayInMs)
{
	sendMidiMessageDelayed(port, message.getByte1(), message.getByte2(), message.getByte3(), delayInMs);
}
