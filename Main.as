//
//  Main.as
//
//  Created by Jim Mazur on 11/17/13.
//	contactjim@jimmazur.com
//

#include "HWController.as"
#include "Sequencer.as"
#include "MidiHelpers.as"
#include "RemixDeckController.as"

//string g_sequencerMidiIncomingPort = "Reaktor 5 Virtual Output";
//string g_sequencerMidiOutgoingPort = "Bome's Midi Translator 1";


// Global variables & Objects

// Name the midi ports to connect to the sequencer here. Mainly we are concerned with connecting the
// incoming port to the device that will be sending the MIDI clock. The outgoing port will be the port
// that recieves the sequenced MIDI events
//string g_sequencerMidiIncomingPort = "Traktor Virtual Output";//
string g_sequencerMidiIncomingPort = "Maoj In";
//string g_sequencerMidiOutgoingPort = "Traktor Virtual Input";
string g_sequencerMidiOutgoingPort = "Moaj Out";

// We need an array of sequencers as we will have individual patterns running for each sound. This array
// will be populated in the main startup function. Here we just setup it up as a global variable.
array<Sequencer> g_arrayOfSequencers;

// Name the midi ports to connect to the HW controller class, in this case we are looking for the
// Traktor Kontrol F1
string g_HWControllerMidiIncomingPort = "Traktor Kontrol F1 - 1 Input";
string g_HWControllerMidiOutgoingPort = "Traktor Kontrol F1 - 1 Output";

bool connected = createMidiPorts(g_sequencerMidiIncomingPort, g_sequencerMidiOutgoingPort);
bool connected2 = connectMidiPorts(g_HWControllerMidiIncomingPort, g_HWControllerMidiOutgoingPort);
//bool connected3 = connectMidiPorts(g_sequencerMidiIncomingPort, "MIDI Monitor (Untitled)");
// Create the HWController object, pass along the name of the MIDI ports to connect to as
// part of the constructor
HWController g_hwController(g_HWControllerMidiIncomingPort, g_HWControllerMidiOutgoingPort, g_sequencerMidiOutgoingPort);
RemixDeckController g_remixDeckController(g_sequencerMidiIncomingPort, g_sequencerMidiOutgoingPort, DeckC);
// This function is called when the script is first executed. 
void main ()
{

	for (int i = 0; i < 4; i++)
	{
		// Create a sequencer object by constructing it with the MIDI ports and passing the midi note 
		// number that it should sequence (in this case 'i')
		Sequencer sequencer(g_sequencerMidiIncomingPort, g_sequencerMidiOutgoingPort, i);
		
		// Insert the sequencer object to the end of the array created in the global variables section
		g_arrayOfSequencers.insertLast( sequencer );
		
		// Tell the seqeuncer the address of the HW controller, it will use this address to inform the
		// HW controller about changes in the sequence.
		g_arrayOfSequencers[i].connectHWController(@g_hwController);
		
		// Likewise, tell the HW controller the address of the sequencer
		g_hwController.connectSequencer(i, @g_arrayOfSequencers[i]);
	}

    return;
}


// This function is called whenever a MIDI message arrives at an enabled input port.
// The input name will contain a string, with the name of the MIDI input port.
// The 3 bytes represent the MIDI data. In the case the MIDI data is less than 3
// bytes than (such as a single byte MIDI clock message) than the second and third bytes 
// will have a 0 value by default.

void handleIncomingMidiMessage (const string &in name, int byte1, int byte2, int byte3)
{
	//DBG("Incoming message from: " name + ": " + byte1 + ", " + byte2 + ", " + byte3);

	// Turn the invidual bytes into a MidiMessage object, making it easier to work with
	MidiMessage message(byte1, byte2, byte3);
	
	//Check if the incoming port name is the same one as being used for the sequencer
	if (name == g_sequencerMidiIncomingPort)
	{
		// Pass the message along to each one of the sequencers in the array
		for (uint i = 0; i < g_arrayOfSequencers.length(); i++)
		{
			g_arrayOfSequencers[i].processMidiMessage(message);
		}
		g_remixDeckController.processMidiMessage(message);
	}
	//Process messages to goto HW controller 
	if (name == g_HWControllerMidiIncomingPort)	g_hwController.processMidiMessage(message);
}

void timerCallback(const string &in timerName)
{

}
