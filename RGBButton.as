enum Brightness
{
	Bright = 127,
	DimBright = 100,
	Dim = 40,
	DimOff = 10,
	Off = 0
}

enum Colors
{
	Red = 0,
	OrangeRed = 5,
	SafetyOrange = 8,
	DarkOrange = 11,
	Orange = 14,
	Amber = 16,
	TangerineYellow = 18,
	GoldenYellow = 20,
	Yellow = 21,
	ChartreuseYellow = 23,
	ElectricLime = 25,
	SpringBud = 28,
	Chartreuse = 32,
	BrightGreen = 35,
	Green = 39,
	Lime = 44,
	SpringGreen = 51,
	Aqua = 61,
	DeepSkyBlue = 67,
	DodgerBlue = 75,
	NavyBlue = 78,
	Blue = 85,
	Indigo = 92,
	Purple = 100,
	Magenta = 106,
	HotMagenta = 110,
	Pink = 117
}

class Color
{
	Color(const uint8 &in hue, const uint8 &in saturation, const uint8 &in brightness)
	{
		m_hue = hue;
		m_saturation = saturation;
		m_brightness = brightness;
	}

	Color(const Color &in other)
	{
		this.m_hue = other.m_hue;
		this.m_saturation = other.m_saturation;
		this.m_brightness = other.m_brightness;
	}

	Color(const uint8 &in color)
	{
		m_hue = color;
		m_saturation = 127;
		m_brightness = Dim;
	}

	Color()
	{
		m_hue = Red;
		m_saturation = 0;
		m_brightness = Dim;
	}

	void setHSB (const uint8 &in hue, const uint8 &in saturation, const uint8 &in brightness)
	{
		m_hue = hue;
		m_saturation = saturation;
		m_brightness = brightness;
	}

	void getHSB (uint8 &in hue, uint8 &in saturation, uint8 &in brightness) const
	{
		hue = m_hue;
		saturation = m_saturation;
		brightness = m_brightness;
	}

	int8 getHue() const
	{
		return m_hue;
	}

	int8 getSaturation() const
	{
		return m_saturation;
	}

	int8 getBrightness() const
	{
		return m_brightness;
	}
	Color getWhite(const uint8 &in brightness = 127)
	{
		return Color(0, 0, brightness);
	}
	Color getAsBrightness(const uint8 &in brightness) const
	{
		return Color(m_hue, m_saturation, brightness);
	}

	private uint8 m_hue;
	private uint8 m_saturation;
	private uint8 m_brightness; 
}

class RGBButton
{
	RGBButton() {}
	RGBButton(const string &in outputPort, const int &in controller, const Color &in color, const uint onBrightness = Bright, const uint offBrightness = Dim)
	{
		m_outputPort = outputPort;
		m_controller = controller;
		m_currentColor = color.getAsBrightness(offBrightness);
		setOnColor(color.getAsBrightness(onBrightness));
		setOffColor(color.getAsBrightness(offBrightness));
	}

	void setColor(const Color &in color)
	{
		m_currentColor = color;
		updatePad();
	}

	Color getColor() const
	{
		return m_currentColor;
	}

	void setBaseColor(const Color &in color, const uint8 onBrightness = Bright, const uint8 offBrightness = Dim)
	{
		setOnColor(color.getAsBrightness(onBrightness));
		setOffColor(color.getAsBrightness(offBrightness));
	}

	void setOnColor(const Color &in color)
	{
		m_onColor = color;
	}

	void setOffColor(const Color &in color)
	{
		m_offColor = color;
	}

	void setOn()
	{
		m_currentColor = m_onColor;
		updatePad();
	}

	void setOff()
	{
		m_currentColor = m_offColor;
		updatePad();
	}

	private void updatePad()
	{
		sendMidiMessage(m_outputPort, 0xB0, m_controller, m_currentColor.getHue() );
		sendMidiMessage(m_outputPort, 0xB1, m_controller, m_currentColor.getSaturation() );
		sendMidiMessage(m_outputPort, 0xB2, m_controller, m_currentColor.getBrightness() );
	}

	private string m_outputPort;
	private int8 m_controller;

	private Color m_onColor;
	private Color m_offColor;
	private Color m_currentColor;
}

class Button
{
	Button() {}
	Button(const string &in outputPort, const int &in controller, const int onBrightness = Bright, const int offBrightness = Dim)
	{
		m_outputPort = outputPort;
		m_controller = controller;
		setOnBrightness(onBrightness);
		setOffBrightness(offBrightness);
	}

	void setBrighntess(const int &in brightness)
	{
		m_currentBrightness = brightness;
		updatePad();
	}

	int getBrightness() const
	{
		return m_currentBrightness;
	}


	void setOnBrightness(const int &in brightness)
	{
		m_onBrightness = brightness;
	}

	void setOffBrightness(const int &in brightness)
	{
		m_offBrightness = brightness;
	}

	void setOn()
	{
		m_currentBrightness = m_onBrightness;
		updatePad();
	}

	void setOff()
	{
		m_currentBrightness = m_offBrightness;
		updatePad();
	}

	private void updatePad()
	{
		sendMidiMessage(m_outputPort, 0xBC, m_controller, m_currentBrightness);
	}

	private string m_outputPort;
	private int8 m_controller;

	private int m_onBrightness;
	private int m_offBrightness;
	private int m_currentBrightness;
}